--Vi Inspired Textual Editor === VITE
-- Version 0.05
-- by: ASCII Benefactor
-- April 2022
-------------------------------------


filename="dummy.txt"
VER="v0.05" 
nc=require("nocurses")
dofile("box.lua")
-- dofile("func.lua")



-- Small Funcs Start ------------------------------

function at(s,p)
    return string.sub(s,p,p)
end

function len(s)
    return string.len(s)
end

----------------------------end small funcs ------
function start()
    nc.setunderline(true)
    print("VITE === Vi Inspired Text Editor === "..VER)
    nc.setunderline(false)
    currline=-1
    currfile=-1
    lines_tbl={}
    cmd()
    --after closing
    if currfile ~= -1 then
        print("Closing VITE...bye!")
        currfile:close()
    else
        print("Closing VITE...bye!")
    end
end

function display_overlay()
  if not currfile == -1 then
    nc.setfontcolor("BLUE")
    nc.gotoxy(1,0)
    topline=currfn.."  "..""
    print(topline)
    
  end
    
end
-- list of Commands 
-- ----------------
-- * = newfile
-- > = appendtext
-- : = main commands
-- = = Display pending lines
-- ? = Display Help


function cmd()
     if currline== -1 then
            print("No file opened")
     end
     while true do
        ---------------------- main loop
     if currline ~= -1 then print(currline..":") end -- InfoLine TM
     nc.setfontcolor("YELLOW")
     inp = io.input():read("*l")
     nc.setfontcolor("WHITE")
        -- all commands start with a symbol 
        if at(inp,1)=="*" and string.len(inp)<=1 then
           print("You must enter a valid filename...")
        elseif at(inp,1)=="*" and string.len(inp)>1 then
            newfn=string.sub(inp,2)
            new_file(newfn)
            print("New File:"..newfn.."\n")
            currline=1
        elseif at(inp,1) == ">" and string.len(inp)==1  then
            -- print("line [#"..currline.."]:")
            table.insert(lines_tbl,currline,"")
            currline=currline+1
            -- if inp has > symbol, then its a literal line to write to file 
        elseif at(inp,1) == ">" and string.len(inp)>1 then  
            table.insert(lines_tbl,currline,string.sub(inp,2))
            currline=currline+1
        elseif at(inp,1) == ":" and at(inp,2)=="l" and string.len(inp)>2 then
              -- TODO MAKE LOAD FUNC TODO
        elseif at(inp,1) == ":" and at(inp,2)=="w" then
           write_file() 
        elseif at(inp,1) == "=" and string.len(inp)==1 then
            display_text()
        elseif at(inp,1)==":" and at(inp,2)=="g" and at(inp,3)=="o" and string.match(inp,"%d+")~= nil then
            currline=string.match(inp,"%d+")
        elseif at(inp,1)=="x" and #inp==1 then
        elseif at(inp,1)=="g" and #inp==1 then
            display_overlay()
        elseif at(inp,1)=="?" and #inp==1 then
            print_help()
        elseif at(inp,1)==":" and at(inp,2)=="q" then 
            break
        else
            -- catchall --
            if inp=="" then 
            else
                nc.setfontcolor("RED")
                print("Unknown Command!")
                nc.setfontcolor("WHITE")
            end
        -- end of command defs =========================
        end      
    end
end

function add_line(lpos,str)
    local str,lpos
    if currfile~=-1 then
        table.insert(lines_tbl,lpos,str)
    else
        print("No file Open!")
    end
end

function write_file() -- flush data to the file":w" 
    if currfile~=-1 then
        currfile:write(table.concat(lines_tbl,"\n").."\n")
        print("Flushed buffer unto "..newfn) 
        -- TODO Show filesize XXX
    else
        print("No file to write to !")
    end
end

function new_file(fn) --also load
    if currfile==-1 then 
        currfile = io.open(fn,"w+")
        currfn=fn
    elseif currfile~=-1 then
        nc.setfontcolor("RED")
        print("Confirm Close file?")
        nc.setfontcolor("WHITE")
        if nc.getch()=="y" or nc.getch()=="Y" then 
            currfile:close()
            currfile = io.open(fn,"w+")
        elseif nc.getch()=="n" then 
            return 0 
        end
    end
end

function display_file() -- Displays file proper
    currfile:seek("set")
    local lnum = 1
    for line in currfile:lines() do
        nc.gotoxy(1,lnum)
        print(lnum..":"..line)
        lnum=lnum+1
    end
end

function print_help()
    local k
    nc.clrscr()
    nc.setfontcolor("GREEN")
    print(" VITE commands ")
    print("===================================")
    print("*                      new file ")
    print(">                    insert line")
    print("=                     Show lines")
    print(":                  main commands")

    print("enter any key to continue..." )
   
    while k == nil do
        k=nc.getch()    
    end
    nc.clrscr()

    print(" Main ':' commands ")
    print("===================================")
    print(":q               quit")
    print(":w      Write pending files to file")
    print("k                     Show lines")
    print(":                  main commands")
    print("enter any key to continue..." )
   
    while k == nil do
        k=nc.getch()    
    end
    print("End of Help")
    nc.setfontcolor("WHITE")
end

function rem_line(lnum)

end

function display_text() -- prints lines_tbl (bef flush)
    currfile:seek("set")
    local line
    nc.clrscr()
    nc.setfontcolor("CYAN")
    for lnum,lstr in pairs(lines_tbl) do
      --  nc.gotoxy(1,k)
    print(lnum..":"..lstr)
    end
    print("=================================\n\n")
    nc.setfontcolor("WHITE")
end
-- Program Loop Begin 
start()
