-- toolbox : various functions
-- ::asciibene::

function draw_pane(ttl)
    local bxlen=#ttl+4
    local bylen=3
    for i=1,bylen do
        if i==1 or i==3 then 
            print(string.rep("*",bxlen))
        elseif i==2 then 
            print("*".." "..ttl.." ".."*")
    
        end
    end
end
